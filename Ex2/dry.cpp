#include "dry.h"

double calcTrianglePerimeter(const Point& A, const Point& B, const Point& C)
{ 

	//the sqrt function repeats itself with no need
	/*return sqrt((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y)) +
		sqrt((C.x - B.x) * (C.x - B.x) + (C.y - B.y) * (A.y - B.y)) +
		sqrt((A.x - C.x) * (A.x - C.x) + (A.y - C.y) * (C.y - C.y));*/
	return  distance(A, B) + distance(A, C) + distance(B, C);
}

double calcPentagonPerimeter(const Point & A, const Point & B, const Point & C, const Point & D, const Point & E)
{
	return distance(A,B) + distance(B,C) + distance(C,D) + distance(D,E) + distance(E,A);
}

/*this function can be used to calculate more shape perimeters
and contains only one line so it is easier to find mistakes*/
double distance(const Point & A, const Point & B)
{
	return sqrt((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));
}

/*can calculate all sorts of perimeters no matter how many points there are*/
double calcPerimeter(const Point points[], int numOfPoints)
{
	double perimeter = 0;
	for (int i = 0; i+1 < numOfPoints; i++)
	{
		perimeter += distance(points[i], points[i + 1]);
	}
	perimeter += distance(points[0], points[numOfPoints - 1]);
	return perimeter;
}



int main(void)
{
	Point p1{ 2, 6 };
	Point p2{4, 5};
	Point p3{ 3, 9 };
	Point p4{ 8, 10 };
	Point p5{ 0, 7 };
	Point points[] = { p1,p2,p3,p4,p5 };
	std::cout << calcPentagonPerimeter(p1, p2, p3,p4,p5) << " , "<<calcPerimeter(points,5) << std::endl;
	system("pause");
	return 0;
}
