#pragma once
#include <math.h>
#include <iostream>
struct  Point
{
	int x;
	int y;
};

double calcTrianglePerimeter(const Point& A, const Point& B, const Point& C);
double calcPentagonPerimeter(const Point& A, const Point& B, const Point& C, const Point& D, const Point& E);
double distance(const Point& A, const Point& B);
double calcPerimeter(const Point points[], int numOfPoints);