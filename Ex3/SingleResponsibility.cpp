#include "email.h"


class Person //person should not check the email it is not person responsibility
{
	std::string _firstName;
	std::string _lastName;
	Email _email;
	
public:
	Person(const std::string& firstName, const std::string& lastName, const std::string& email)
		: _firstName(firstName), _lastName(lastName)
	{
		
		_email.setEmail(email);
	}
};



