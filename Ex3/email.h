#pragma once
#include <string>

class Email
{
public:
	Email()
	{

	}
	void setEmail(const std::string& email)
	{
		
		if (!validateEmail(email))
			throw std::invalid_argument("email address not valid");
		_email = email;
		
	}
	//Email should check email str
	bool validateEmail(const std::string& email)
	{
		if (email.find('@') == std::string::npos)
			return false;
		return true;
	}

private:
	std::string _email;
};