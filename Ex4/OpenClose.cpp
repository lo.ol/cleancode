#define PI 3.14159265359
#include <math.h>
struct Shape
{
	
	virtual double calcArea(Shape* shape) const = 0;
};

struct Circle : public Shape
{
	int x;
	int y;
	int radius;
	
	double calcArea(Circle* c)

	{
		return PI * pow(c->radius, 2);
	}
};
struct Rectangle : public Shape
{
	int x1;
	int y1;
	int x2;
	int y2;
	double calcArea(Rectangle* r)
	{
		
		return (r->x2 - r->x1) * (r->y2 - r->y1);
	}
};
