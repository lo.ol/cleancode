#include <vector>
/*function returns an average of vales in given vector*/
float average(const std::vector<int>& l)
{
	if (l.empty())
		throw std::exception("invalid");
	float sum = 0.0;
	for (std::vector<int>::const_iterator it = l.begin(), e = l.end(); it != e; ++it)
		sum += *it;
	return sum / l.size();
}