#include <iostream>
class Instrument
{
public:
	virtual void playChord() const = 0;
	virtual bool canPlay() const = 0;
};


class Guitar : public Instrument
{
public:
	void playChord() const
	{
		
		std::cout << "Am" << std::endl;
	}
	bool CanPlay() const
	{
		return true;
	}

};
class Drum : public Instrument
{
	void playChord() const
	{
		// We throw exception here because drum can't play chords
		throw std::runtime_error("unsupported instrument");
	}
	bool CanPlay() const
	{
		return false;
	}

};

void Musician(const Instrument& instrument)
{
	if(instrument.canPlay())
		instrument.playChord();
	
}