#include "Digit.h"



Digit::Digit()
{
}


Digit::~Digit()
{
}

bool Digit::isNum(string digit)
{
	if (*(digit.begin()) == '0') return false; //check if first char is 0
	for (auto it = digit.begin(); it != digit.end(); it++)
	{
		if (*it < '0' || *it > '9') //check if all chars are digits
			return false;
	}
	return true;
}

void Digit::setDigit(string digit)
{
	if (isNum(digit))
	{
		this->_num = digit;
	}
	else 
	{
		throw exception("Invalid Num!\n");
	}
}
