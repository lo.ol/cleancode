#pragma once
#include <string>
#include <iostream>
#include <exception>
using namespace std;

class Digit
{
public:
	Digit();
	~Digit();
	bool isNum(string digit);
	void setDigit(string digit);
private:
	string _num;
};

