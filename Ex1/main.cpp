#include "Digit.h"

int main(void)
{
	std::string input;
	
	std::cout << "Please enter digit: ";
	std::getline(std::cin, input); //getting Digit
	try
	{
		Digit i;
		i.setDigit(input); 
	}
	catch (exception& error)
	{
		cout << error.what();
	}

	//no need in digit class function isNum can check the digit string without digit class

	system("pause");
	return 0;
}