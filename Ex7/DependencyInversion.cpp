#include <string>

class Email
{
public:
	Email()
	{
	}
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	void sendEmail() const
	{
		// Send email
	}
};

class SMS
{
public:
	SMS()
	{

	}
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	void sendSms() const
	{
		// Send sms
	}
};

/*reminder uses email and any other class with dependency becuase
to create email we need params from reminder, 
also to add more types of reminders we need to change all of it's functions*/
class Reminder
{
	Email *_pEmail;
	SMS *_pSms;

public:
	Reminder(const std::string& to,
		const std::string& from,
		const std::string& subject,
		const std::string& content) : _pEmail(nullptr),_pSms(nullptr)
	{
		_pEmail = new Email();
		_pEmail->to(to);
		_pSms = new SMS();
		_pSms->to(to);
	}
	~Reminder()
	{
		delete _pEmail;
		delete _pSms;
	}
	void sendReminder() const
	{
		_pEmail->sendEmail();
		_pSms->sendSms();
	}
};