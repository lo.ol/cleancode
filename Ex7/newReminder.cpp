#include <string>

class Send
{
public:
	virtual void send() const = 0;
	virtual void to(const std::string& to) = 0;
	virtual void from(const std::string& from) = 0;
	virtual void content(const std::string& content) = 0;
	virtual void subject(const std::string& subject) = 0;

};


class Email : public Send
{
public:
	Email()
	{
	}
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	void send() const
	{
		// Send email
	}
};

class SMS : public Send
{
public:
	SMS()
	{

	}
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void content(const std::string& content) {}
	void subject(const std::string& content) {}
	void send() const
	{
		// Send sms
	}
};


class Reminder
{
	Send *_pmsg;
	

public:
	/*const std::string& to,
	const std::string& from,
		const std::string& subject,
		const std::string& content*/
	Reminder(Send*& toRemind) : _pmsg(nullptr)
	{
		_pmsg = toRemind;
		
	}
	~Reminder()
	{
		
		
	}
	void sendReminder() const
	{
		_pmsg->send();
		
	}
};