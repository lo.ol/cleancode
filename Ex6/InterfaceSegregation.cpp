#include <iostream>
//not right
struct IPhone1
{
	virtual void call() const = 0;
	virtual void sms() const = 0; //not used in home phone
	virtual void fax() const = 0; //not used in smart phone
};

struct IPhone
{
	virtual void call() const = 0; //every phone can call
};

struct HomePhone : public IPhone
{
public:
	
		virtual void call()
		{
			std::cout << "E.T. Phone Home" << std::endl;
		}
		//no need in sms 
		void fax()
		{
			std::cout << "E.T. Fax Home" << std::endl;
		}
};

struct SmartPhone : public IPhone
{
	virtual void call()
	{
		std::cout << "E.T. Smart Phone" << std::endl;
	}
	void sms()
	{
		std::cout << "E.T. SMS Smart Phone" << std::endl; 
	}
	//no need in fax
};