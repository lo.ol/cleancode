﻿#include <string>
#include <algorithm>
#define MIN_NAME_LENGTH 2
#define MAX_NAME_LENGTH 20

bool isUniqueName(const std::string& name)
{
	// in a real word scenario we would have checked against a list of names
	return true;
}
bool isNotValid(char ch)
{
	return !isalpha(ch) && !isdigit(ch) && ch != '!' && ch != '$' && ch != '.';
}

/*The function checks whether the entered name is a correct name and if so, does it already exist
returns a Boolean value indicating whether the name is correct and did not appear before.
For the name to be correct it must contain at least MIN_NAME_LENGTH and no more than MAX_LENGTH.
Also, the name should begin with a letter. The rest of the characters in the name should be numbers or letters.
If the name meets all the conditions it will be checked against other names enteredץ
then will be returned if it is a valid name to use.*/
bool isValidUserName(const std::string& username)
{
	bool valid = false;
	if (username.length() >= MIN_NAME_LENGTH)
	{
		if (username.length() <= MAX_NAME_LENGTH)
		{
			if (isalpha(username[0]) && isdigit(username[1]))
			{
				bool foundNotValidChar = std::find_if(username.begin(), username.end(), isNotValid) != username.end();
				if (!foundNotValidChar)
				{
					valid = isUniqueName(username);
				}
			}
		}
	}
	return valid;
}

/*the problem in the code is: to change validity of user name we need to change existing ifs or add more 
also to change is NotValid function that checks only one char each time*/