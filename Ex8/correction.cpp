#include <string>
#include <algorithm>
#define MIN_NAME_LENGTH 2
#define MAX_NAME_LENGTH 20

bool isUniqueName(const std::string& name)
{
	// in a real word scenario we would have checked against a list of names 
	return true;
}

bool isNotValid(const std::string& username)
{
	for (auto ch = username.begin(); ch != username.end(); ch++)
	{
		if (!isalpha(*ch) && !isdigit(*ch) && *ch != '$' && *ch != '!' && *ch != '.') return true;
	}
	return false;
}

bool isValidUserName(const std::string& username)
{
	bool valid = false;
	if (username.length() < MIN_NAME_LENGTH || username.length() > MAX_NAME_LENGTH) return valid;
	else if (!isalpha(username[0]) || !isdigit(username[1])) return valid;
		
	
	if (!isNotValid(username))
	{
			valid = isUniqueName(username);
	}
		
	return valid;
}